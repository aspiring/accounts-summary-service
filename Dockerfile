FROM openjdk:11
ARG JAR_FILE
COPY $JAR_FILE accounts-summary.jar
ENTRYPOINT ["java", "-jar", "/accounts-summary.jar"]
