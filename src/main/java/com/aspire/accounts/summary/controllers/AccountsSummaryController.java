package com.aspire.accounts.summary.controllers;

import com.aspire.accounts.summary.domain.AccountsSummary;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountsSummaryController {

    @GetMapping(value = "/accounts/summary", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountsSummary> getAccountsSummary() {
        return null;
    }
}
