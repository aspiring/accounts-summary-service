package com.aspire.accounts.summary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountsSummaryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountsSummaryServiceApplication.class, args);
    }

}
